/**
 * @module utils-common/date
 */

/**
 * 把时间转为指定合适字符串
 * @since 1.0.0
 * @author 潘知悦
 * @param  date 时间
 * @param fmt 格式 
 * @return 格式化后的时间字符串
 * 
 * @示例1
 * ```js
 * import {format} from 'utils-common/date'
 * 
 * format(new Date(),"yyyy-MM-dd hh:mm:ss")
 * //2020-12-30 15:56:26
 * ```
 */
export function format(date: Date, fmt: string): string {
    let o:any = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (let k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

/**
 * 把时间转换成几分钟前，几小时前，几天前
 * @since 1.0.0
 * @author 潘知悦
 * @param  date 时间
 * @示例1
 * ```js
 * import {formatMsgTime} from 'utils-common/date'
 * 
 * formatMsgTime(new Date(new Date().getTime() - 1000 * 60 * 5*10))
 * //结果:50分钟前
 * ```
 */
export function formatMsgTime(date: Date): string {
    let timespan = date.getTime();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let minute = date.getMinutes();
    let second = date.getSeconds();
    let now = new Date();
    let now_new = now.getTime();  //typescript转换写法

    let milliseconds = 0;
    let timeSpanStr;

    milliseconds = now_new - timespan;
    if (milliseconds <= 1000 * 60 * 1) {
        timeSpanStr = '刚刚';
    }
    else if (1000 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60) {
        timeSpanStr = Math.round((milliseconds / (1000 * 60))) + '分钟前';
    }
    else if (1000 * 60 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24) {
        timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60)) + '小时前';
    }
    else if (1000 * 60 * 60 * 24 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24 * 15) {
        timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60 * 24)) + '天前';
    }
    else if (milliseconds > 1000 * 60 * 60 * 24 * 15 && year == now.getFullYear()) {
        timeSpanStr = month + '-' + day + ' ' + hour + ':' + minute;
    } else {
        timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;
    }
    return timeSpanStr;
}


/**
 * 获取指定时间往前一段时间
 * @since 1.0.0
 * @author 潘知悦
 * @param date 指定时间
 * @param year 前移年
 * @param month 前移月
 * @param day 前移日
 * @param hour 前移小时
 * @param minute 前移分钟
 * @param second 前移秒
 * @return 修改后的时间
 * @示例1
 * ```js
 * import {getPrevTime} from 'utils-common/date'
 * 
 * getPrevTime(Date.Now,0,0,0,1,1,1);
 * ```
 */
export function getPrevTime(date: Date, year: number, month: number, day: number, hour: number, minute: number, second: number): Date {
    let newDate = new Date();

    newDate.setFullYear(date.getFullYear() - year);
    newDate.setMonth(date.getMonth() - month - 1);
    newDate.setDate(date.getDate() - day);
    newDate.setHours(date.getHours() - hour);
    newDate.setMinutes(date.getMinutes() - minute);
    newDate.setSeconds(date.getSeconds() - second);
    return newDate;
}


/**
 * 获取指定时间往后一段时间22
 * @since 1.0.0
 * @author 潘知悦
 * @param date 指定时间
 * @param year 后移年
 * @param month 后移月
 * @param day 后移日
 * @param hour 后移小时
 * @param minute 后移分钟
 * @param second 后移秒
 * @return 修改后的时间
 * @示例1
 * ```js
 * import {getNextTime} from 'utils-common/date'
 *
 * getNextTime(Date.Now,0,0,0,1,1,1);
 * ```
 */
export function getNextTime(date: Date, year: number, month: number, day: number, hour: number, minute: number, second: number): Date {
    let newDate = new Date();

    newDate.setFullYear(date.getFullYear() + year);
    newDate.setMonth(date.getMonth() + month-1);
    newDate.setDate(date.getDate() + day);
    newDate.setHours(date.getHours() + hour);
    newDate.setMinutes(date.getMinutes() + minute);
    newDate.setSeconds(date.getSeconds() + second);
    return newDate;
}
