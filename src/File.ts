/**
 * @module utils-common/file
 */


/**
 * 获取文件名,带后缀<br>
 * 内容:之前只考虑了分隔符为\的清空,把\替换为/,然后再获取文件名称(网络路径一般使用/作为分隔符)
 * @param filePath 文件路径
 * @return 文件名
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {getFileName} from 'utils-common/file'
 * let fileName=getFileName("d:\\file.txt");
 * //结果:file.txt
 * ```
 */
 export function getFileName(filePath:string):string {
    filePath = filePath.replace(/\\/g, "/");
    return filePath.substring(filePath.lastIndexOf("/") + 1);
}


/**
 * 获取文件名,不带后缀
 * @param filePath 文件路径
 * @return 不带后缀的文件名
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {getFileNameNotExt} from 'utils-common/file'
 * let fileNameNotExt=getFileNameNotExt("d:\\file.txt");
 * //结果:file
 * ```
 */
export function getFileNameNotExt(filePath:string):string {
    let fileName = getFileName(filePath);
    return fileName.substring(0, fileName.lastIndexOf("."));
}

/**
 * 获取文件后缀
 * @param filePath 文件路径
 * @return 后缀
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {getExt} from 'utils-common/file'
 * let ext=getExt("d:\\file.txt");
 * //结果:txt
 * ```
 */
export function getExt(filePath:string):string {
    let fileName = getFileName(filePath);
    return fileName.substring(fileName.lastIndexOf(".") + 1);
}


/**
 * 文件大小转换/单位换算
 * @param size 文件大小
 * @param decimal 小数位数
 * @param units 单位列表
 * @param maxSize 最大值，达到这个数值进入下一个单位
 * @return
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {formatSize} from 'utils-common/file'
 * const size = formatSize(4564654);
 * //结果:4.35MB
 * ```
 */
export function formatSize(size:number, decimal:number = 2, units:string[] = ['B', 'KB', 'MB', 'GB', 'TB'], maxSize:number = 1024):string {
    let unit = units[0];
    let i = 0;
    while (i < units.length && size >= maxSize) {
        i++;
        size = size / maxSize;
        unit = units[i];
    }
    return (unit === 'B' ? size : size.toFixed(decimal)) + unit;
}
