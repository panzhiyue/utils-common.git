/**
 * @module utils-common/string
 */
//#region String对象方法

/**
 * indexOf() 方法返回 指定值 在字符串对象中首次出现的位置。
 * @param  str   字符串
 * @param  searchStr 查询字符串
 * @param fromIndex 从 fromIndex 位置开始查找
 * @return 返回 指定值 在字符串对象中首次出现的位置,不存在返回-1
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {indexOf} from "utils-common/string"
 * 
 * indexOf("abc123def666","123")   3
 * indexOf("abc123def666","123",5)   -1
 * ```
 */
export function indexOf(str: string, searchStr: string, fromIndex?: number): number {
    return str.indexOf(searchStr, fromIndex);
}


/**
 * search() 正则方法  执行一个查找，看该字符串对象与一个正则表达式是否匹配。
 * @param str   字符串
 * @param rex 正则表达式
 * @return 返回字符串与正则表达式首次匹配的位置,不存在返回-1
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {search} from "utils-common/string"
 * 
 * search("abc123def666","6")    9
 * search("abc123def666",/[0-9]/)    3
 * ```
 */
export function search(str: string, rex: string|RegExp): number {
    return str.search(rex);
}


/**
 * replace() 替换方法  把字符串中指定值用其他值替换(只替换首次匹配到的)
 * @param  str   字符串
 * @param  replaced 被替换的对象
 * @param  replaceStr 用于替换的字符串
 * @return  返回替换后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {replace} from "utils-common/string"
 * 
 * replace("abc123def666", "6", "7")    abc123def766
 * replace("abc123def666",/[0-9]/,"t")    abct23def666
 * ```
 */
export function replace(str: string, replaced: string | RegExp, replaceStr: string): string {
    return str.replace(replaced, replaceStr);
}

/**
 * match  
 * 当字符串匹配到正则表达式（regular expression）时,match() 方法会提取匹配项。
 * @param str 字符串
 * @param rex 正则表达式
 * @return 与正则表达式匹配的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {match} from "utils-common/string"
 * 
 * match("abc123def666", /\d+/)   ["123", index: 3, input: "abc123def666", groups: undefined]
 * match("abc123def666", /\d/)  ["1", index: 3, input: "abc123def666", groups: undefined]
 * ```
 */
export function match(str: string, rex: RegExp): RegExpMatchArray|null {
    return str.match(rex);
}

/**
 * 通过把字符串分割成子字符串来把一个 String 对象分割成一个字符串数组。
 * @param  str 输入字符串
 * @param  rex 分割字符串
 * @return  分割后的字符串数组
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {split} from "utils-common/string"
 * 
 * split("abc123def666", /\d+/);  ["abc", "def", ""]
 * split("abc123def666", /\d/);   ["abc", "", "", "def", "", "", ""]
 * split("abc123def666", 1);  ["abc", "23def666"]
 * ```
 */
export function split(str: string, rex: string | RegExp): string[] {
    return str.split(rex);
}

/**
 * 提取字符串中的一部分，并返回这个新的字符串
 * @param  str 字符串
 * @param  startIndex  起始索引(包含)
 * @param  endIndex 结束索引(不包含)
 * @return  子字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {splice} from "utils-common/string"
 * 
 * slice("abc123def666",1, 4); // bc1
 * ```
 */
export function slice(str: string, startIndex: number, endIndex: number): string {
    return str.slice(startIndex, endIndex);
}


/**
 * 方法返回字符串中从指定位置开始到指定长度的子字符串。
 * @param  str 字符串
 * @param  startIndex  起始索引(包含)
 * @param length 长度
 * @return  子字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {substr} from "utils-common/string"
 * 
 * substr("abc123def666",1, 4); // bc12
 * ```
 */
export function substr(str: string, startIndex: number, length: number) {
    return str.substr(startIndex, length);
}


/**
 * 返回字符串两个索引之间（或到字符串末尾）的子串。
 * @param  str 字符串
 * @param startIndex  起始索引(包含)
 * @param endIndex 结束索引(不包含)
 * @return 子字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {substring} from "utils-common/string"
 * 
 * substring("abc123def666",1, 4); // bc1
 * ```
 */
export function substring(str: string, startIndex: number, endIndex: number): string {
    return str.substring(startIndex, endIndex);
}

/**
 * 删除一个字符串两端的空白字符
 * @param  str 字符串
 * @return 删除两端空格后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {trim} from "utils-common/string"
 * 
 * trim("   abc123    def666    "); abc123    def666
 * ```
 */
export function trim(str: string): string {
    return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g, '');
}


/**
 * 将字符串转换成小写并返回
 * @param str 字符串
 * @return 转为小写后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {toLowerCase} from "utils-common/string"
 * 
 * toLowerCase("aaaaA"); aaaaa
 * ```
 */
export function toLowerCase(str: string): string {
    return str.toLowerCase();
}


/**
 * 将字符串转为大写形式，并返回。
 * @param str 输入字符串
 * @return 转为大写后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {toUpperCase} from "utils-common/string"
 * 
 * console.log(toUpperCase("aaaaA")); AAAAA
 * ```
 */
export function toUpperCase(str: string): string {
    return str.toUpperCase();
}

//#endregion

/**
 * 删除一个字符串左侧的空白字符
 * @param str 输入字符串
 * @return  删除左侧空格后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {ltrim} from "utils-common/string"
 * 
 * console.log(ltrim("  aaa  aa    "));  //"aaa  aa    "
 * ```
 */
export function ltrim(str: string): string {
    return str.replace(/(^\s*)/g, "");
}

/**
 * 删除一个字符串右侧的空白字符
 * @param str 输入字符串
 * @return 删除右侧空格后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {rtrim} from "utils-common/string"
 * 
 * console.log(rtrim("  aaa  aa    "));  //"  aaa  aa"
 * ```
 */
export function rtrim(str: string): string {
    return str.replace(/(\s*$)/g, "");
}

/**
 * 字符串拼接
 * @param formatStr 格式字符串
 * @param  strs 填充字符串
 * @return  结果字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {format} from "utils-common/string"
 * 
 * format("{0}字符串拼接{1}","测试","函数"); //"测试字符串拼接函数"
 * ```
 */
export function format(formatStr: string, ...strs: string[]): string {
    for (let i = 0; i < strs.length; i++) {
        while (formatStr.indexOf("{" + (i) + "}") > -1) {
            formatStr = formatStr.replace("{" + (i) + "}", strs[i]);
        }
    }
    return formatStr;
}


/**
 * 缩短字符串,多余部分用指定后缀ext表示
 * @param  string 输入字符串
 * @param  length 字节长度
 * @param  ext 超出长度部分替换后缀,默认为...
 * @return  缩短后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {shortString} from "utils-common/string"
 * 
 * shortString("aaaaaaaaa",3);  //aaa...
 * console.log(shortString("啊啊啊啊啊啊啊啊啊",5,"_____")); //啊啊_____
 * ```
 */
export function shortString(str: string, len: number, ext: string = "..."): string {
    if (str == "" || str == null || str == undefined || len == 0) {
        return "";
    }
    let shortStr = "",
        count = 0;

    for (let i = 0; i < str.length; i++) {
        let char = str.charAt(i);
        count += getCharLength(char);
        if (count <= len) {
            shortStr += char;
        } else {
            shortStr += ext;
            break;
        }
    }
    return shortStr;
}
/**
 * 获取字符串的字节长度
 * @param str 字符串
 * @return 字符串的字节长度
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {getStringLengnth} from "utils-common/string"
 * 
 * getStringLength("aaa");  //3
 * getStringLength("啊啊啊");  //6
 * ```
 */
export function getStringLength(str: string): number {
    if (!str) {
        return 0;
    }
    let length = 0;
    for (let i = 0; i < str.length; i++) {
        length += getCharLength(str.charAt(i));
    }
    return length;
}

/**
 * 获取字符的字节长度
 * @param  char 单个字符
 * @return 字符的字节长度
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {getCharLength} from "utils-common/string"
 * 
 * console.log(getCharLength("a"));  //1
 * console.log(getCharLength("啊"));  //2
 * ```
 */
export function getCharLength(char: string): number {
    return char.replace(/[\u0391-\uffe5]/g, "aa").length;
}

/**
 * 右补位
 * @param oriStr  原字符串
 * @param len  目标字符串长度
 * @param alexin  补位字符
 * @return  补位后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {padRight} from "utils-common/string"
 * 
 * padRight("aa",10,'0');  //aa00000000
 * padRight("aa",1,'0');  //aa
 * ```
 */
export function padRight(oriStr: string, len: number, alexin: string): string {
    let strlen = oriStr.length;
    let str = "";
    if (strlen < len) {
        for (let i = 0; i < len - strlen; i++) {
            str = str + alexin;
        }
    }
    str = oriStr + str;
    return str;
}

/**
 * 左补位
 * @param oriStr  原字符串
 * @param len  目标字符串长度
 * @param alexin  补位字符
 * @return 补位后的字符串
 * @since 1.0.0
 * @author 潘知悦
 * @示例1
 * ```js
 * import {padLeft} from "utils-common/string"
 * 
 * padLeft("aa",10,'0');  //00000000aa
 * padLeft("aa",1,'0');  //aa
 * ```
 */
export function padLeft(oriStr: string, len: number, alexin: string): string {
    let strlen = oriStr.length;
    let str = "";
    if (strlen < len) {
        for (let i = 0; i < len - strlen; i++) {
            str = str + alexin;
        }
    }

    str = str + oriStr;
    return str;
}



