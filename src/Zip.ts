/**
 * Zip压缩,解压并下载
 * 作者:潘知悦
 * 日期:2020年8月6日
 * 依赖:JsZip,JsZip-Utils,FileSaver
 * @link https://github.com/Stuk/jszip
 * @link https://github.com/Stuk/jszip-utils
 * 
 * @module utils-common/Zip
 **/

import ZipN from "jszip";
import {saveAs} from 'file-saver';
import * as FileHelper from "./file"
import JSZipUtils from "jszip-utils"

/**
 * 文件压缩
 * @param  files  file控件上传的文件列表或文件路径列表
 * @param  callback 回调函数
 * @param fileNames 压缩包中文件名称(与files中一一对应,没有则用原有文件名)
 * @since 1.0.0
 * @author 潘知悦
 */
export function zipFiles(files: FileList | string[], callback: Function, fileNames?: string[]) {
    if (files instanceof Array) {
        zipFilesHttp(files, callback, fileNames);
    } else {
        zipFilesInput(files, callback, fileNames);
    }
}

/**
 * 文件压缩并下载
 * @param files  file控件上传的文件列表或文件路径列表
 * @param zipName 压缩包文件名
 * @param fileNames 压缩包中文件名称(与files中一一对应,没有则用原有文件名)
 * @since 1.0.0
 * @author 潘知悦
 */
export function zipFiles_DownLoad(files: FileList | string[], zipName: string, fileNames?: string[]) {
    zipFiles(files, (content: any) => {
        saveAs(content, zipName);
    }, fileNames);
}


/**
 * 文件压缩(通过file控件上传的文件)
 * @param files  file控件上传的文件列表
 * @param callback 回调函数
 * @param fileNames 压缩包中文件名称(与files中一一对应,没有则用原有文件名)
 * @since 1.0.0
 * @author 潘知悦
 */
function zipFilesInput(files: FileList, callback?: Function, fileNames?: string[]) {
    console.log(9999);
    var zip = new ZipN();

    //promise数组
    let promiseArr: Promise<any>[] = [];
    for (let i = 0; i < files.length; i++) {
        let file = files[i];
        let name = file.name;
        if (fileNames != null && fileNames[i] && fileNames[i] != "") {
            name = fileNames[i];
        }
        promiseArr.push(new Promise(function (resolve) {
            let reader = new FileReader();
            reader.onload = function (evt: any) {
                resolve({
                    name: name,
                    data: evt.target.result
                });
            }
            reader.readAsArrayBuffer(file);
        }));
    }

    //文件加载完成
    Promise.all(promiseArr).then((results) => {
        for (let i = 0; i < results.length; i++) {
            let result = results[i];
            zip.file(result.name, result.data);
        }
        zip.generateAsync({
            type: "blob"
        })
            .then(function (content) {
                if (callback) {
                    callback(content);
                }

            });
    })
        .catch(function (error) {
            console.error(error);
        });
}



/**
 * 文件压缩(通过文件路径)
 * @param paths 文件路径列表
 * @param callback 回调函数
 * @param fileNames 压缩包中文件名称(与files中一一对应,没有则用原有文件名)
 * @since 1.0.0
 * @author 潘知悦
 */
function zipFilesHttp(paths: string[], callback?: Function, fileNames?: string[]) {
    console.log(888);
    var zip = new ZipN();

    let promiseArr: Promise<any>[] = [];
    for (let i = 0; i < paths.length; i++) {
        let path = paths[i];
        let name = FileHelper.getFileName(path);
        if (fileNames && fileNames[i] && fileNames[i] != "") {
            name = fileNames[i];
        }
        promiseArr.push(new Promise(function (resolve, reject) {
            JSZipUtils.getBinaryContent(path, function (error: any, data: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve({
                        name: name,
                        data: data
                    });
                }
            });
        }));
    }

    Promise.all(promiseArr).then(function (results) {
        for (let i = 0; i < results.length; i++) {
            let result = results[i];
            zip.file(result.name, result.data);
        }
        zip.generateAsync({
            type: "blob"
        })
            .then(function (content) {
                if (callback) {
                    callback(content);
                }
            });
    });;
}

/**
 * zip解压(通过file控件上传的压缩包)
 * @param zipFile file控件上传的文件对象或zip文件路径
 * @param callback 回调函数
 * @since 1.0.0
 * @author 潘知悦
 */
export function unZip(zipFile: File | string, callback: Function) {
    if (typeof (zipFile) == 'string') {
        unZipHttp(zipFile, callback);

    } else {
        unZipInput(zipFile, callback);
    }
}

/**
 * zip解压并下载文件
 * @param zipFile file控件上传的文件对象或zip文件路径
 * @since 1.0.0
 * @author 潘知悦
 */
export function unZip_DownLoad(zipFile: File | string) {
    unZip(zipFile, (files: any) => {
        for (let field in files) {
            let file = files[field];
            file.async("blob").then(function (content: any) {
                saveAs(content, file.name);
            });
        }
    });
}


/**
 * zip解压(通过file控件上传的压缩包)
 * @param zipFile file控件上传的文件
 * @param callback 回调函数
 * @since 1.0.0
 * @author 潘知悦
 */
function unZipInput(zipFile: File, callback: Function) {
    let reader = new FileReader();
    reader.onload = function (evt: any) {
        ZipN.loadAsync(evt.target.result).then((obj: any) => {
            let files = obj.files;
            callback(files);
        });
    }
    reader.readAsArrayBuffer(zipFile);
}

/**
 * zip解压(文件路径)
 * @param zipFile zip文件路径
 * @param callback 回调函数
 * @since 1.0.0
 * @author 潘知悦
 */
function unZipHttp(zipFile: string, callback: Function) {
    JSZipUtils.getBinaryContent(zipFile, function (error: any, data: any) {
        if (error) {
            console.error(error);
        } else {
            ZipN.loadAsync(data).then((obj) => {
                let files = obj.files;
                callback(files);
            });
        }
    });
}