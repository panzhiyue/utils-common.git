/**
 * 是否为Internet Explorer 版本（不是 Edge）
 */
export const ie: boolean = 'ActiveXObject' in window;

/**
 * 是否为低于ie9的Internet Explorer 版本
 */
export const ielt9: boolean = ie && !document.addEventListener;

/**
 * 是否为 Edge 网络浏览器。
 */
export const edge: boolean = 'msLaunchUri' in navigator && !('documentMode' in document);

/**
 * 是否为基于 webkit 的浏览器，如 Chrome 和 Safari（包括移动版本）。
 */
export const webkit: boolean = userAgentContains('webkit');

/**
 * 是否为android平台浏览器
 */
export const android: boolean = userAgentContains('android');

/**
 * 是否为android2,3平台浏览器
 */
export const android23: boolean = userAgentContains('android 2') || userAgentContains('android 3');

/* See https://stackoverflow.com/a/17961266 for details on detecting stock Android */
const webkitVer = parseInt(/WebKit\/([0-9]+)|$/.exec(navigator.userAgent)![1], 10); // also matches AppleWebKit


/**
 * 是否为 Android 股票浏览器（即非 Chrome）
 */
export const androidStock: boolean = android && userAgentContains('Google') && webkitVer < 537 && !('AudioNode' in window);

/**
 * 是否为Opera 浏览器
 */
export const opera: boolean = !!(<any>window).opera;

/**
 * 是否为Chrome 浏览器
 */
export const chrome: boolean = !edge && userAgentContains('chrome');

/**
 * 是否为火狐等基于gecko-based的浏览器
 */
export const gecko: boolean = userAgentContains('gecko') && !webkit && !opera && !ie;

/**
 * 是否为Safari浏览器
 */
export const safari: boolean = !chrome && userAgentContains('safari');

function userAgentContains(str) {
    return navigator.userAgent.toLowerCase().indexOf(str) >= 0;
}