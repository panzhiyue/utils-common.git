/**
 * 坐标相关方法
 */

import { factorial, parseDecimal } from "../math"
import { Extent } from "./extent"
import * as turf from "@turf/turf"

/**
 * 坐标[x,y]
 */
export type Coordinate = number[];



/**
 * 检测两个经纬度是否相同
 * @param coordinate1 坐标1
 * @param coordinate2 坐标2
 * @return
 */
export const equal = (coordinate1: Coordinate, coordinate2: Coordinate): boolean => {
    if (coordinate1 === coordinate2) return true
    const [x1, y1] = coordinate1
    const [x2, y2] = coordinate2
    return parseDecimal(x1) === parseDecimal(x2) && parseDecimal(y1) === parseDecimal(y2);
}



/**
 * 返回边界内的随机位置
 * @param extent [-180, -90, 180, 90]
 */
export const randomCoordinate = (extent: Extent): Coordinate => {
    return turf.randomPosition(extent as turf.BBox);
}

/**
 * 生成随机线的坐标集合
 * @param opt_options 
 * @param opt_options.bbox 边界
 * @param opt_options.num_vertices
 * @param opt_options.max_length
 * @param opt_options.max_rotation
 */
export const randomLineString = (opt_options: { bbox?: turf.BBox, num_vertices?: number, max_length?: number, max_rotation?: number }): Array<Coordinate> => {
    let options = Object.assign({ bbox: [-180, -90, 180, 90], num_vertices: 10, max_length: 0.0001, max_rotation: Math.PI / 8 }, opt_options);
    var lineStrings = turf.randomLineString(1, options);
    return lineStrings[0].coordinates;
}

/**
 * 生成随机面的坐标集合
 * @param opt_options 
 * @param opt_options.bbox 边界
 * @param opt_options.num_vertices
 * @param opt_options.max_radial_length
 */
export const randomPolygon = (opt_options: { bbox?: turf.BBox, num_vertices?: number, max_radial_length?: number }): Array<Array<Coordinate>> => {
    let options = Object.assign({ bbox: [-180, -90, 180, 90], num_vertices: 10, max_radial_length: 10 }, opt_options);
    var polygons = turf.randomPolygon(1, options);
    return polygons[0].coordinates;
}

