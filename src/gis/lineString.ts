import { factorial, parseDecimal } from "../math"
import { Coordinate } from "./coordinate";
import * as turf from "@turf/turf"
import { Constants } from "./utils";


/**
 * 返回沿该线指定距离的点。
 * @param coordinates 线的点集合
 * @param distance 距离
 * @param options
 * @return 坐标点
 */
export const getCoordinateByDistance = (coordinates: Array<Coordinate>, distance: number, options: { units?: turf.Units } = { units: "degrees" }): Coordinate => {
    var line = turf.lineString(coordinates);

    var along: turf.Feature<turf.Point> = turf.along(line, distance, options);
    return along.geometry.coordinates as Coordinate;
}

/**
 * 返回沿该线指定比例的点。
 * @param coordinates 线的点集合
 * @param scale 比例 0-1
 * @param options
 * @return 坐标点
 */
export const getCoordinateByScale = (coordinates: Array<Coordinate>, scale: number, options: { units?: turf.Units } = { units: "degrees" }): Coordinate => {
    let line = turf.lineString(coordinates);
    let length = turf.length(line, options);
    let distance = length * scale;

    return getCoordinateByDistance(coordinates, distance);
}

/**
 * 计算点到线段最短间距的点
 * @param coordinates 
 * @param coordinate
 * @return 
 */
export const getCoordinateByPoint = (coordinates: Array<Coordinate> | Array<Array<Coordinate>>, coordinate: Coordinate): Coordinate => {

    let options: { units?: turf.Units } = { units: "degrees" }

    let cs: Array<Array<Coordinate>>;
    if (coordinate[0][0] instanceof Array) {
        cs = coordinates as Array<Array<Coordinate>>;
    } else {
        cs = [coordinates] as Array<Array<Coordinate>>;
    }
    let line = turf.multiLineString(cs);
    let pt = turf.point(coordinate);

    var nearestPoint = turf.nearestPointOnLine(line, pt, options);
    return nearestPoint.geometry.coordinates as Coordinate;
}

/**
 * 根据一条线、起点和终点，返回这些点之间的线段。起止点不需要正好落在直线上。
 * @param coordinates 
 * @param startPt 
 * @param stopPt 
 * @return 
 */
export const sliceByPoint = (coordinates: Array<Coordinate>, startPt: Coordinate, stopPt: Coordinate): Array<Coordinate> => {
    let line = turf.lineString(coordinates);
    let start = turf.point(startPt);
    let stop = turf.point(stopPt);
    let sliced = turf.lineSlice(start, stop, line);
    return sliced.geometry.coordinates as Array<Coordinate>;
}

/**
 * 取一条线，沿该线到起始点的指定距离，以及沿该线到终止点的指定距离，并返回这些点之间的该线的分段。
 * @param coordinates 
 * @param startDist 
 * @param stopDist 
 * @param options 
 * @return 
 */
export const sliceByDistance = (coordinates: Array<Coordinate>, startDist: number, stopDist: number, options: { units?: turf.Units } = { units: "degrees" }): Array<Coordinate> => {
    let start = getCoordinateByDistance(coordinates, startDist, options);
    let stop = getCoordinateByDistance(coordinates, stopDist, options);
    return sliceByPoint(coordinates, start, stop);
}

/**
 * 取一条线，沿该线到起始点的指定比例，以及沿该线到终止点的指定比例，并返回这些点之间的该线的分段。
 * @param coordinates 
 * @param startScale 
 * @param stopScale 
 * @param options 
 * @return 
 */
export const sliceByScale = (coordinates: Array<Coordinate>, startScale: number, stopScale: number, options: { units?: turf.Units } = { units: "degrees" }): Array<Coordinate> => {
    let start = getCoordinateByScale(coordinates, startScale, options);
    let stop = getCoordinateByScale(coordinates, stopScale, options);
    return sliceByPoint(coordinates, start, stop);
}



/**
 * 生成贝塞尔曲线
 * @param points 点集合(数量必须大于等于2)
 * @param space 必须要大于0
 * @return 贝塞尔曲线点集
 */
export const createBezierCurve = (points: Array<Coordinate> = [], space: number = 0.01): Array<Coordinate> => {
    // 大于2个点才能有曲线
    if (points.length <= 2 || space <= 0) return points

    let x: number = 0
    let y: number = 0
    // 控制点个数
    const n: number = points.length - 1
    const line: Array<Coordinate> = [];
    for (let t = 0; t < 1; t = t + space) {
        x = 0;
        y = 0;
        for (let i = 0; i <= n; i++) {
            const [x1, y1] = points[i]
            x += (factorial(n) / (factorial(i) * factorial(n - i))) * Math.pow((1 - t), n - i) * Math.pow(t, i) * x1;
            y += (factorial(n) / (factorial(i) * factorial(n - i))) * Math.pow((1 - t), n - i) * Math.pow(t, i) * y1;
        }
        line.push([x, y]);
    }
    line.push(points[points.length - 1])
    return line;
}

/**
 * 创建曲线
 * @param from 起点
 * @param to 终点
 * @param radius 半径
 * @param angle 角度
 * @param space 
 * @return 曲线点集
 */
export const createCurve = (from: Coordinate, to: Coordinate, radius: number = 0, angle: number = 90, space: number = 0.01): Array<Coordinate> => {
    const [fx, fy] = from
    const [tx, ty] = to
    // 获取圆心坐标
    const ox = (fx + tx) / 2
    const oy = (fy + ty) / 2
    // 线与x轴的夹角
    const dx = (fy - ty) / (fx - tx)
    // 参照直线的夹角
    const r = (angle * Math.PI / 180) + Math.atan(dx)
    // console.log('r', r)
    // 计算得出曲线拐点的坐标
    const x = ox + radius * Math.cos(r)
    const y = oy + radius * Math.sin(r)
    // 得到了线的曲度控制点
    const line = [from, [x, y] as Coordinate, to]
    // 生成曲线的路径坐标集合
    return createBezierCurve(line, space)
}

/**
 * 线性插值（二维坐标）,每隔一段长度插值
 * @param coordinates  线条坐标数组，支持曲线
 * @param space 点的间隔距离，经纬度单位，值越小，增加的点越多
 * @return 插值后线坐标数组
 */
export const linearInterpolate = (coordinates: Array<Coordinate> = [], space: number = 0.01): Array<Coordinate> => {
    const points: Array<Coordinate> = []
    for (let i = 0; i < coordinates.length; i++) {
        if (i >= coordinates.length - 1) break
        const [x1, y1] = coordinates[i]
        const [x2, y2] = coordinates[i + 1]
        // 计算线与x轴的角度
        const k = Math.atan2(Math.abs(y1 - y2), Math.abs(x1 - x2))
        // 0 ~ 90 度之间
        const angle = k * (180 / Math.PI)
        points.push([x1, y1])
        // 大于45度用维度计算，小于45度用经度计算
        if (angle > 45) {
            const diff = space * Math.sign(y2 - y1)
            let y = y1 + diff
            while (diff > 0 ? y < y2 : y > y2) {
                const x = (y - y1) * (x2 - x1) / (y2 - y1) + x1
                points.push([x, y])
                y += diff
            }
        } else {
            const diff = space * Math.sign(x2 - x1)
            let x = x1 + diff
            while (diff > 0 ? x < x2 : x > x2) {
                const y = (y2 - y1) / (x2 - x1) * (x - x1) + y1
                points.push([x, y])
                x += diff
            }
        }
        points.push([x2, y2])
    }
    return [...new Set(points)]
}

/**
 * 获取指定圆弧点集
 * @param center 中心点
 * @param radius 半径
 * @param startAngle 起始弧度
 * @param endAngle 结束弧度
 * @return
 */
export function getArc(center: Coordinate, radius: number, startAngle: number, endAngle: number): Array<Coordinate> {
    var x: number, y: number, pnts: Array<Coordinate> = [];
    var angleDiff = endAngle - startAngle;
    angleDiff = angleDiff < 0 ? angleDiff + Constants.TWO_PI : angleDiff;
    for (var i = 0; i <= Constants.FITTING_COUNT; i++) {
        var angle = startAngle + angleDiff * i / Constants.FITTING_COUNT;
        x = center[0] + radius * Math.cos(angle);
        y = center[1] + radius * Math.sin(angle);
        pnts.push([x, y]);
    }
    return pnts;
};