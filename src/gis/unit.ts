/**
 * 单位相关
 */

import * as turf from "@turf/turf"
import { Coordinate } from "./coordinate"

//#regioon 单位

/**
 * 长度单位
 */
export enum LengthUnin {
    MILES = 'miles',  //英里
    NAUTICALMILES = 'nauticalmiles',  //航海英里
    INCHES = 'inches',  //英寸
    YARDS = 'yards',  //码
    METERS = 'meters',  //米
    KILOMETERS = 'kilometers',  //公里
    CENTIMETERS = 'centimeters',  //厘米
    FEET = 'feet',  //英尺
}
//#endregion 单位


/**
 * 转换从正北方向(顺时针正方向)开始的任何方位角度，并返回0-360度(顺时针正方向)之间的角度，0为正北
 * @param bearing  -180°到180°之间的角度
 * @returns 0°-360°之间的角度
 * @示例
 * ``` js
 * bearingToAzimuth(-45)
 * //结果:315
 * ```
 */
export const bearingToAzimuth = (bearing: number): number => {
    return turf.bearingToAzimuth(bearing);
}

/**
 * 将角度转换成弧度
 * @param degrees 角度
 * @returns 传入角度degrees对应的弧度
 * @示例
 * ``` js
 * degreesToRadians(60)
 * //结果:1.0471975511965976
 * ```
 */
export const degreesToRadians = (degrees: number): number => {
    return turf.degreesToRadians(degrees);
}

/**
 * 将弧度转换成角度
 * @param radians 弧度
 * @returns 0°-360°之间的角度
 * @示例
 * ``` js
 * radiansToDegrees(1.0471975511965976)
 * //结果:60
 * ```
 */
export const radiansToDegrees = (degrees: number): number => {
    return turf.degreesToRadians(degrees);
}
