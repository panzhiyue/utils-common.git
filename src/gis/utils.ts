import { Coordinate } from "./coordinate";

export const Constants = {
    TWO_PI: Math.PI * 2,
    HALF_PI: Math.PI / 2,
    FITTING_COUNT: 100,      //圆的点数
    ZERO_TOLERANCE: 0.0001   //容差
};

/**
 * 2个点之间的距离
 * @param pnt1  第一个点
 * @param pnt2 第二个点
 * @return 距离
 */
export function getDistance(pnt1: Coordinate, pnt2: Coordinate): number {
    return Math.sqrt(Math.pow((pnt1[0] - pnt2[0]), 2) + Math.pow((pnt1[1] - pnt2[1]), 2));
};

/**
 * 线长度
 * @param points 点集
 * @return 长度
 */
export function getLength(points: Array<Coordinate>): number {
    let distance2: number = 0;
    for (var i = 0; i < points.length - 1; i++)
        distance2 += getDistance(points[i], points[i + 1]);
    return distance2;
};

/**
 * 线长度的0.99次方
 * @param points 点集
 * @return 长度
 */
export function getBaseLength(points: Array<Coordinate>): number {
    return Math.pow(getLength(points), 0.99);
};

/**
 * 获取2点的中点
 * @param pnt1 点1
 * @param pnt2 点2
 * @return 2点的中点
 */
export function getMid(pnt1: Coordinate, pnt2: Coordinate): Coordinate {
    return [(pnt1[0] + pnt2[0]) / 2, (pnt1[1] + pnt2[1]) / 2];
};

/**
 * 通过3个点获取圆的中心点
 * @param pnt1 第一个点
 * @param pnt2 第二个点
 * @param pnt3 第三个点
 * @return 圆中心点
 */
export function getCircleCenterOfThreePoints(pnt1: Coordinate, pnt2: Coordinate, pnt3: Coordinate): Coordinate {
    var pntA = [(pnt1[0] + pnt2[0]) / 2, (pnt1[1] + pnt2[1]) / 2];
    var pntB = [pntA[0] - pnt1[1] + pnt2[1], pntA[1] + pnt1[0] - pnt2[0]];
    var pntC = [(pnt1[0] + pnt3[0]) / 2, (pnt1[1] + pnt3[1]) / 2];
    var pntD = [pntC[0] - pnt1[1] + pnt3[1], pntC[1] + pnt1[0] - pnt3[0]];
    return getIntersectPoint(pntA, pntB, pntC, pntD);
};


/**
 * 获取2条线段的交点
 * @param pngA 线段1的起点
 * @param pngB 线段1的终点
 * @param pngC 线段2的起点
 * @param pngD 线段2的终点
 * @return 2条线段的交点
 */
export function getIntersectPoint(pntA: Coordinate, pntB: Coordinate, pntC: Coordinate, pntD: Coordinate): Coordinate {
    if (pntA[1] == pntB[1]) {
        var f = (pntD[0] - pntC[0]) / (pntD[1] - pntC[1]);
        var x = f * (pntA[1] - pntC[1]) + pntC[0];
        var y = pntA[1];
        return [x, y];
    }
    if (pntC[1] == pntD[1]) {
        var e = (pntB[0] - pntA[0]) / (pntB[1] - pntA[1]);
        x = e * (pntC[1] - pntA[1]) + pntA[0];
        y = pntC[1];
        return [x, y];
    }
    e = (pntB[0] - pntA[0]) / (pntB[1] - pntA[1]);
    f = (pntD[0] - pntC[0]) / (pntD[1] - pntC[1]);
    y = (e * pntA[1] - pntA[0] - f * pntC[1] + pntC[0]) / (e - f);
    x = e * y - e * pntA[1] + pntA[0];
    return [x, y];
};

/**
 * 获取线段方位角
 * @param startPnt 起点
 * @param endPnt  终点
 * @return 方位角
 */
export function getAzimuth(startPnt: Coordinate, endPnt: Coordinate): number {
    var azimuth;
    var angle = Math.asin(Math.abs(endPnt[1] - startPnt[1]) / getDistance(startPnt, endPnt));
    if (endPnt[1] >= startPnt[1] && endPnt[0] >= startPnt[0])
        azimuth = angle + Math.PI;
    else if (endPnt[1] >= startPnt[1] && endPnt[0] < startPnt[0])
        azimuth = Constants.TWO_PI - angle;
    else if (endPnt[1] < startPnt[1] && endPnt[0] < startPnt[0])
        azimuth = angle;
    else if (endPnt[1] < startPnt[1] && endPnt[0] >= startPnt[0])
        azimuth = Math.PI - angle;
    return azimuth;
};

/**
 * 获取3点构成的夹角
 * @param pntA
 * @param pntB
 * @param pntC
 * @return 3点构成的夹角弧度
 */
export function getAngleOfThreePoints(pntA: Coordinate, pntB: Coordinate, pntC: Coordinate): number {
    var angle = getAzimuth(pntB, pntA) - getAzimuth(pntB, pntC);
    return (angle < 0 ? angle + Constants.TWO_PI : angle);
};

/**
 * 判断是是否为顺时针
 * @param pnt1 点1
 * @param pnt2 点2
 * @param pnt3 点3
 * @return ture为顺时针,false为逆时针
 */
export function isClockWise(pnt1: Coordinate, pnt2: Coordinate, pnt3: Coordinate): boolean {
    return ((pnt3[1] - pnt1[1]) * (pnt2[0] - pnt1[0]) > (pnt2[1] - pnt1[1]) * (pnt3[0] - pnt1[0]));
};


/**
 * 根据线段，夹角，长度获取点
 * @param startPnt  起点
 * @param endPnt    终点
 * @param angle     夹角弧度
 * @param distance  长度
 * @param clockWise true:顺时针,false:逆时针
 */
export function getThirdPoint(startPnt: Coordinate, endPnt: Coordinate, angle: number, distance: number, clockWise: boolean = true): Coordinate {
    var azimuth = getAzimuth(startPnt, endPnt);
    var alpha = clockWise ? azimuth + angle : azimuth - angle;
    var dx = distance * Math.cos(alpha);
    var dy = distance * Math.sin(alpha);
    return [endPnt[0] + dx, endPnt[1] + dy];
};