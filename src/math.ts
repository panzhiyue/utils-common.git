/**
 * 数学相关方法
 */


/**
 * 阶乘
 * @param num 输入数值
 * @return num的阶乘
 */
export const factorial = (num: number): number => {
    if (num <= 1) {
        return 1;
    } else {
        return num * factorial(num - 1);
    }
}

/**
 * 格式化数字，修复精度问题
 * @param number
 * @return
 */
export const parseDecimal = (number: number): number => {
    return parseFloat(number.toFixed(12))
}