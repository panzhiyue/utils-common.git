/**
 * Counter for getUid.
 */
 let uidCounter_: number = 0;

 /**
  * Gets a unique ID for an object. This mutates the object so that further calls
  * with the same object as a parameter returns the same value. Unique IDs are generated
  * as a strictly increasing sequence. Adapted from goog.getUid.
  */
 export function getUid(obj: any): number {
     return obj.pzy_uid || (obj.pzy_uid = String(++uidCounter_));
 }