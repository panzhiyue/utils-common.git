
/**
 * 验证电子邮箱格式是否正确
 * @param emailAddress 电子邮箱地址
 * @return 是否为邮箱格式
 */
export function isEmail(emailAddress: string) {
    let emailReg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    return emailAddress && emailReg.test(emailAddress);
}

/**
 * 验证手机号码格式是否正确
 * @param phoneNumber 手机号码
 * @return 是否为手机号码格式
 */
export function isPhoneNumber(phoneNumber: string) {
    let phoneReg = /^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/;
    return phoneNumber && phoneReg.test(phoneNumber);
}

/**
 * 验证字符串是否包含特殊字符
 * @param str 输入字符串
 * @return 是否包含特殊字符
 */
export function containSpecial(str: string) {
    let reg = RegExp(/[(\ )(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\-)(\_)(\+)(\=)(\[)(\])(\{)(\})(\|)(\\)(\;)(\:)(\')(\")(\,)(\.)(\/)(\<)(\>)(\?)(\)]+/);
    return (reg.test(str));
}

/**
 * 验证字符串是否包含大写字母
 * @param str 输入字符串
 * @return 是否包含大写字母
 */
export function containUpperCase(str: string) {
    let reg = /[A-Z]+/;
    return reg.test(str);
}

/**
 * 验证字符串是否包含小写字母
 * @param str 输入字符串
 * @return 是否包含小写字母
 */
export function containLowerCase(str: string) {
    let reg = /[a-z]+/;
    return reg.test(str);
}

/**
 * 验证字符串是否包含数字
 * @param str 输入字符串
 * @return 是否包含数字
 */
export function containNumber(str: string) {
    let reg = /[0-9]+/;
    return reg.test(str);
}

/**
 * 验证字符串是否包含中文
 * @param str 输入字符串
 * @return 是否包含中文
 */
export function containChinese(str: string) {
    let reg = /[\u4e00-\u9fa5]+/;
    return reg.test(str);
}